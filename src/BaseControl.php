<?php

declare(strict_types=1);

namespace Skadmin\Meeting;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'meeting';
    public const DIR_IMAGE = 'meeting';

    public function getMenu() : ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-handshake']),
            'items'   => ['overview'],
        ]);
    }
}
