<?php

declare(strict_types=1);

namespace Skadmin\Meeting\Components\Admin;

use App\Components\Form\FormWithUserControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\DateTime;
use Skadmin\File\Components\Admin\CreateComponentFormFile;
use Skadmin\File\Components\Admin\FileDownloadByFacade;
use Skadmin\File\Components\Admin\FileRemoveByFacade;
use Skadmin\File\Components\Admin\FormFile;
use Skadmin\File\Components\Admin\IFormFileFactory;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Meeting\BaseControl;
use Skadmin\Meeting\Doctrine\Meeting\Meeting;
use Skadmin\Meeting\Doctrine\Meeting\MeetingFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function explode;
use function is_bool;

/**
 * Class Edit
 */
class Edit extends FormWithUserControl
{
    use APackageControl;
    use CreateComponentFormFile;
    use FileDownloadByFacade {
        CreateComponentFormFile::checkFileObject insteadof FileDownloadByFacade;
    }
    use FileRemoveByFacade {
        CreateComponentFormFile::checkFileObject insteadof FileRemoveByFacade;
    }

    /** @var LoaderFactory */
    private $webLoader;

    /** @var MeetingFacade */
    private $facade;

    /** @var Meeting */
    private $meeting;

    /** @var ImageStorage */
    private $imageStorage;

    /** @var FormFile */
    private $formFile;

    public function __construct(?int $id, MeetingFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage, IFormFileFactory $iFormFileFactory, FileStorage $fileStorage)
    {
        parent::__construct($translator, $user);
        $this->facade       = $facade;
        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->meeting     = $this->facade->get($id);
        $this->fileObject  = $this->meeting;
        $this->fileStorage = $fileStorage;

        if (! $this->meeting->isLoaded()) {
            return;
        }

        $this->formFile = $iFormFileFactory->create();
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->meeting->isLoaded()) {
            return new SimpleTranslation('meeting.edit.title - %s', $this->meeting->getName());
        }

        return 'meeting.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss() : array
    {
        $css = [
            $this->webLoader->createCssLoader('daterangePicker'),
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];

        if ($this->meeting->isLoaded()) {
            foreach ($this->formFile->getCss() as $wl) {
                $css[] = $wl;
            }
        }

        return $css;
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        $js = [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('moment'),
            $this->webLoader->createJavaScriptLoader('daterangePicker'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];

        if ($this->meeting->isLoaded()) {
            foreach ($this->formFile->getJs() as $wl) {
                $js[] = $wl;
            }
        }

        return $js;
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        /**
         * @var DateTime $termFrom
         * @var DateTime $termTo
         */
        [$termFrom, $termTo] = Arrays::map(explode(' - ', $values->term), static function (string $date) : DateTime {
            $date = DateTime::createFromFormat('d.m.Y', $date);
            return is_bool($date) ? new DateTime() : $date;
        });

        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->image_preview, BaseControl::DIR_IMAGE);

        if ($this->meeting->isLoaded()) {
            if ($identifier !== null && $this->meeting->getImagePreview() !== null) {
                $this->imageStorage->delete($this->meeting->getImagePreview());
            }

            $meeting = $this->facade->update(
                $this->meeting->getId(),
                $values->name,
                $values->content,
                $termFrom,
                $termTo,
                $values->place_name,
                $values->place_address,
                $values->place_gps_lat,
                $values->place_gps_lng,
                $identifier
            );
            $this->onFlashmessage('form.meeting.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $meeting = $this->facade->create(
                $values->name,
                $values->content,
                $termFrom,
                $termTo,
                $values->place_name,
                $values->place_address,
                $values->place_gps_lat,
                $values->place_gps_lng,
                $identifier
            );
            $this->onFlashmessage('form.meeting.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $meeting->getId(),
        ]);
    }

    public function processOnBack() : void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render() : void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->meeting = $this->meeting;
        $template->render();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.meeting.edit.name')
            ->setRequired('form.meeting.edit.name.req');
        $form->addTextArea('content', 'form.meeting.edit.content');

        // MEETING
        $form->addText('term', 'form.meeting.edit.term')
            ->setRequired('form.meeting.edit.term.req')
            ->setHtmlAttribute('data-daterange');
        $form->addImageWithRFM('image_preview', 'form.meeting.edit.image-preview');

        // PLACE
        $form->addText('place_name', 'form.meeting.edit.place-name');
        $form->addText('place_address', 'form.meeting.edit.place-address');
        $form->addText('place_gps_lat', 'form.meeting.edit.place-gps-lat');
        $form->addText('place_gps_lng', 'form.meeting.edit.place-gps-lng');

        // BUTTON
        $form->addSubmit('send', 'form.meeting.edit.send');
        $form->addSubmit('send_back', 'form.meeting.edit.send-back');
        $form->addSubmit('back', 'form.meeting.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        if (! $this->meeting->isLoaded()) {
            return [];
        }

        return [
            'name'          => $this->meeting->getName(),
            'content'       => $this->meeting->getContent(),
            'term'          => $this->meeting->getTermFromTo(),
            'place_name'    => $this->meeting->getPlaceName(),
            'place_address' => $this->meeting->getPlaceAddress(),
            'place_gps_lat' => $this->meeting->getPlaceGpsLat(),
            'place_gps_lng' => $this->meeting->getPlaceGpsLng(),
        ];
    }
}
