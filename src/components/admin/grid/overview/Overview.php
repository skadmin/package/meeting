<?php

declare(strict_types=1);

namespace Skadmin\Meeting\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\Meeting\BaseControl;
use Skadmin\Meeting\Doctrine\Meeting\Meeting;
use Skadmin\Meeting\Doctrine\Meeting\MeetingFacade;
use Skadmin\Translator\Translator;
use function sprintf;

/**
 * Class Overview
 */
class Overview extends GridControl
{
    use APackageControl;

    /** @var MeetingFacade */
    private $facade;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(MeetingFacade $facade, Translator $translator, User $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);

        $this->facade       = $facade;
        $this->imageStorage = $imageStorage;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle() : string
    {
        return 'meeting.overview.title';
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()->orderBy('a.termFrom', 'DESC'));

        // DATA
        $translator = $this->translator;
        $dialYesNo  = Arrays::map(Constant::DIAL_YES_NO, static function ($text) use ($translator) : string {
            return $translator->translate($text);
        });

        // COLUMNS
        $grid->addColumnText('name', 'grid.meeting.overview.name')
            ->setRenderer(function (Meeting $meeting) : Html {
                if ($meeting->getImagePreview() !== null) {
                    $img = $this->imageStorage->fromIdentifier([$meeting->getImagePreview(), '36x36', 'exact']);

                    $elImg = Html::el('img', ['src' => '/' . $img->createLink()]);
                }

                $link = $this->getPresenter()->link('Component:default', [
                    'package' => new BaseControl(),
                    'render'  => $this->isAllowed(BaseControl::RESOURCE, 'write') ? 'edit' : 'detail',
                    'id'      => $meeting->getId(),
                ]);

                $href = Html::el('a', [
                    'href'  => $link,
                    'class' => 'font-weight-bold',
                ]);

                if (isset($elImg)) {
                    $href->addHtml($elImg)
                        ->addText(' ');
                }

                $href->addText($meeting->getName());

                return $href;
            });
        $grid->addColumnText('placeName', 'grid.meeting.overview.place-name')
            ->setRenderer(static function (Meeting $meeting) : string {
                $icon = Html::el('i', ['class' => 'fas fa-map-marker-alt']);

                $href = Html::el('a', [
                    'href'   => $meeting->getPlaceLinkToMap(),
                    'target' => '_blank',
                ])->setHtml($icon);

                return sprintf('%s %s', $href, $meeting->getPlaceName());
            })->setTemplateEscaping(false);
        $grid->addColumnText('term', 'grid.meeting.overview.term')
            ->setRenderer(static function (Meeting $meeting) : string {
                return $meeting->getTermClever();
            });
        $grid->addColumnText('NumberOfUsers', 'grid.meeting.overview.number-of-users')
            ->setAlign('center');

        // FILTER
        $grid->addFilterText('name', 'grid.meeting.overview.name');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.meeting.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        $grid->addAction('detail', 'grid.meeting.overview.action.detail', 'Component:default', ['id' => 'id'])->addParameters([
            'package' => new BaseControl(),
            'render'  => 'detail',
        ])->setIcon('eye')
            ->setClass('btn btn-xs btn-default btn-outline-primary');

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.meeting.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        return $grid;
    }
}
