<?php

declare(strict_types=1);

namespace Skadmin\Meeting\Components\Front;

use App\Components\Grid\TemplateControl;
use App\Model\System\APackageControl;
use SkadminUtils\ImageStorage\ImageStorage;
use Skadmin\Meeting\BaseControl;
use Skadmin\Meeting\Doctrine\Meeting\MeetingFacade;
use Skadmin\Translator\Translator;

/**
 * Class Detail
 */
class Overview extends TemplateControl
{
    use APackageControl;

    /** @var MeetingFacade */
    private $facade;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(MeetingFacade $facade, Translator $translator, ImageStorage $imageStorage)
    {
        parent::__construct($translator);
        $this->facade       = $facade;
        $this->imageStorage = $imageStorage;
    }

    public function getTitle(): string
    {
        return 'meeting.front.overview.title';
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');

        $template->meetings    = $this->facade->getMeetingsInFuture();
        $template->oldMeetings = $this->facade->getMeetingsInPast();

        $template->package     = new BaseControl();

        $template->render();
    }
}
