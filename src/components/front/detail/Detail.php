<?php

declare(strict_types=1);

namespace Skadmin\Meeting\Components\Front;

use App\Components\Grid\TemplateControl;
use App\Model\Doctrine\User\User;
use App\Model\Doctrine\User\UserFacade;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\Security\User as LoggedUser;
use Skadmin\File\Components\Admin\FileDownloadByFacade;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Meeting\BaseControl;
use Skadmin\Meeting\Doctrine\Meeting\Meeting;
use Skadmin\Meeting\Doctrine\Meeting\MeetingFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;

/**
 * Class Detail
 */
class Detail extends TemplateControl
{
    use APackageControl;
    use FileDownloadByFacade;

    /** @var MeetingFacade */
    private $facade;

    /** @var UserFacade */
    private $facadeUser;

    /** @var User */
    private $user;

    /** @var Meeting */
    private $meeting;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(int $id, MeetingFacade $facade, UserFacade $facadeUser, LoggedUser $user, Translator $translator, ImageStorage $imageStorage, FileStorage $fileStorage)
    {
        parent::__construct($translator);
        $this->facade       = $facade;
        $this->facadeUser   = $facadeUser;
        $this->imageStorage = $imageStorage;

        $this->user        = $this->facadeUser->get($user->getId());
        $this->meeting     = $this->facade->get($id);
        $this->fileObject  = $this->meeting;
        $this->fileStorage = $fileStorage;
    }

    public function getTitle() : SimpleTranslation
    {
        return new SimpleTranslation('meeting.front.detail.title - %s', $this->meeting->getName());
    }

    public function render() : void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/detail.latte');

        $template->meeting = $this->meeting;
        $template->package = new BaseControl();

        $template->loggedUser = $this->user;

        $template->render();
    }

    public function handleLogin(int $id) : void
    {
        if ($this->facade->addUser($id, $this->user)) {
            $this->onFlashmessage('meeting.front.detail.flash.success.login', Flash::SUCCESS);
        } else {
            $this->onFlashmessage('meeting.front.detail.flash.danger.login', Flash::DANGER);
        }

        $this->redrawDetail();
    }

    private function redrawDetail() : void
    {
        $this->redrawControl('snipUsers');
        $this->redrawControl('snipUsersControl');
        $this->redrawControl('snipNumberOfUsers');
    }

    public function handleLogout(int $id) : void
    {
        $this->facade->removeUser($id, $this->user);
        $this->onFlashmessage('meeting.front.detail.flash.success.logout', Flash::SUCCESS);
        $this->redrawDetail();
    }
}
