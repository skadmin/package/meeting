<?php

declare(strict_types=1);

namespace Skadmin\Meeting\Doctrine\Meeting;

use SkadminUtils\DoctrineTraits\Facade;
use App\Model\Doctrine\User\User;
use App\Model\Doctrine\User\UserFacade;
use DateTimeInterface;
use Nette\Utils\DateTime;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\File\Doctrine\File\File;

/**
 * Class MeetingFacade
 */
final class MeetingFacade extends Facade
{
    /** @var UserFacade */
    private $facadeUser;

    public function __construct(EntityManagerDecorator $em, UserFacade $facadeUser)
    {
        parent::__construct($em);
        $this->table = Meeting::class;

        $this->facadeUser = $facadeUser;
    }

    public function create(string $name, string $content, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placName, string $placeAddress, string $placeGpsLat, string $placeGpsLng, ?string $imagePreview) : Meeting
    {
        return $this->update(null, $name, $content, $termFrom, $termTo, $placName, $placeAddress, $placeGpsLat, $placeGpsLng, $imagePreview);
    }

    public function update(?int $id, string $name, string $content, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placName, string $placeAddress, string $placeGpsLat, string $placeGpsLng, ?string $imagePreview) : Meeting
    {
        $meeting = $this->get($id);
        $meeting->update($name, $content, $termFrom, $termTo, $placName, $placeAddress, $placeGpsLat, $placeGpsLng, $imagePreview);

        $this->em->persist($meeting);
        $this->em->flush();

        return $meeting;
    }

    public function get(?int $id = null) : Meeting
    {
        if ($id === null) {
            return new Meeting();
        }

        $user = parent::get($id);

        if ($user === null) {
            return new Meeting();
        }

        return $user;
    }

    /**
     * @return Meeting[]
     */
    public function getMeetingsInFuture(?int $limit = null) : array
    {
        $qb = $this->getModel();
        $qb->where('a.termFrom >= :termFrom')
            ->setParameter('termFrom', (new DateTime())->setTime(0, 0, 0))
            ->orderBy('a.termFrom');

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Meeting[]
     */
    public function getMeetingsInPast(?int $limit = null) : array
    {
        $qb = $this->getModel();
        $qb->where('a.termTo < :termTo')
            ->setParameter('termTo', (new DateTime())->setTime(0, 0, 0))
            ->orderBy('a.termTo', 'DESC');

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int|Meeting $meeting
     * @param int|User    $user
     */
    public function addUser($meeting, $user) : bool
    {
        if (! $meeting instanceof Meeting) {
            $meeting = $this->get($meeting);
        }

        if (! $user instanceof User) {
            $user = $this->facadeUser->get($user);
        }

        if ($meeting->addUser($user)) {
            $this->em->flush();
            return true;
        }

        return false;
    }

    /**
     * @param int|Meeting $meeting
     * @param int|User    $user
     */
    public function removeUser($meeting, $user) : void
    {
        if (! $meeting instanceof Meeting) {
            $meeting = $this->get($meeting);
        }

        if (! $user instanceof User) {
            $user = $this->facadeUser->get($user);
        }

        $meeting->removeUser($user);
        $this->em->flush();
    }

    public function addFile(Meeting $meeting, File $file) : void
    {
        $meeting->addFile($file);
        $this->em->flush();
    }

    public function getFileByHash(Meeting $meeting, string $hash) : ?File
    {
        return $meeting->getFileByHash($hash);
    }

    public function removeFileByHash(Meeting $meeting, string $hash) : void
    {
        $meeting->removeFileByHash($hash);
        $this->em->flush();
    }
}
