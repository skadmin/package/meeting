<?php

declare(strict_types=1);

namespace Skadmin\Meeting\Doctrine\Meeting;

use App\Model\Doctrine\User\User;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Skadmin\File\Doctrine\File\File;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Meeting
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Term;
    use Entity\PlaceOnTheMap;
    use Entity\ImagePreview;

    /** @var ArrayCollection<int, User> */
    #[ORM\ManyToMany(targetEntity: User::class)]
    #[ORM\JoinTable(name: 'meeting_user')]
    private $users;

    /** @var ArrayCollection<int, File> */
    #[ORM\ManyToMany(targetEntity: File::class)]
    private $files;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    public function addUser(User $user) : bool
    {
        if ($this->isUserLogged($user)) {
            return false;
        }

        $this->users->add($user);
        return true;
    }

    public function getNumberOfUsers() : int
    {
        return $this->users->count();
    }

    public function removeUser(User $user) : void
    {
        $this->users->removeElement($user);
    }

    public function isUserLogged(User $user) : bool
    {
        return $this->users->contains($user);
    }

    /**
     * @return ArrayCollection|File[]
     */
    public function getFiles()
    {
        return $this->files;
    }

    public function update(string $name, string $content, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placeName, string $placeAddress, string $placeGpsLat, string $placeGpsLng, ?string $imagePreview) : void
    {
        $this->name     = $name;
        $this->content  = $content;
        $this->termFrom = $termFrom;
        $this->termTo   = $termTo;

        $this->placeName    = $placeName;
        $this->placeAddress = $placeAddress;
        $this->placeGpsLat  = $placeGpsLat;
        $this->placeGpsLng  = $placeGpsLng;

        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }
    }

    public function addFile(File $file) : void
    {
        $this->files->add($file);
    }

    public function removeFileByHash(string $hash) : void
    {
        $file = $this->getFileByHash($hash);

        if ($file !== null) {
            $this->files->removeElement($file);
        }
    }

    public function getFileByHash(string $hash) : ?File
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq('hash', $hash));
        $file     = $this->files->matching($criteria)->first();

        return $file ? $file : null;
    }
}
