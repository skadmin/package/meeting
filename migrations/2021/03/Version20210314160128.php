<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210314160128 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'meeting.overview', 'hash' => 'ea35f87c478ead85a48a41cfd47fda43', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Setkání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.meeting.title', 'hash' => 'ecd9f8b1c3f215789cda92f88823b5cc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Setkání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.meeting.description', 'hash' => '0221fe12da0994b4039b4689ccadf6b8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat setkání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'meeting.overview.title', 'hash' => '7f09b579fa8f92f3bf8f1b94506ede1a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Setkání|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.meeting.overview.action.new', 'hash' => 'f28eba793409d842d0df357abb395c10', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit setkání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.meeting.overview.name', 'hash' => 'bc2cb0b48dd7754e6ae05cb0f9da1d71', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.meeting.overview.place-name', 'hash' => '3a3b2e8a3da3b42eb976b08ca0eaee32', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Místo konání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.meeting.overview.term', 'hash' => '25355b0cf94704146f77bdfb246251ee', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.meeting.overview.number-of-users', 'hash' => '5a82ddebe449c77e9f12a03fb8180e01', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet účastníků', 'plural1' => '', 'plural2' => ''],
            ['original' => 'meeting.edit.title', 'hash' => '8702c2ab339fd81b8af015df485ac34f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení setkání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.name', 'hash' => 'd2ac024dd078041997fe8a6bcd87a1f1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.name.req', 'hash' => '0b5402bcae7291a935e125d64d5ae576', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.term', 'hash' => 'efd6ce540bc2063313aa1998e5b37e0c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.term.req', 'hash' => '74f97790d005143db2421df6c7eb9e01', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.place-name', 'hash' => '1940e4b49f8b556908fba8d143663215', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název místa konání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.place-address', 'hash' => '6017629569539a57e405c75501b4030d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Adresa', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.place-gps-lat', 'hash' => '59a257af32395956b83a17e7e4ce1722', 'module' => 'admin', 'language_id' => 1, 'singular' => 'GPS šířka', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.place-gps-lng', 'hash' => 'b80523f8e18f51f77d1951e73721f277', 'module' => 'admin', 'language_id' => 1, 'singular' => 'GPS výška', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.image-preview', 'hash' => 'c22476dac86f0c97ba1e935d2d5f3887', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled setkání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.image-preview.rule-image', 'hash' => 'b2f7a4d0b53f3d0f6941d9ebbbfbd315', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.content', 'hash' => '70e5f143ff2eb57de48d1d2e19aea2ec', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.send', 'hash' => '0c2ff3ba7238d273cc875d21321b91b9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.send-back', 'hash' => 'efafb8d07f3a69677adb10499b910c43', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.back', 'hash' => '05a34e9c16b5b948f585b6d30852eb6d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'meeting.edit.title - %s', 'hash' => '32cd5071b5bafbd85faa998c0153b881', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace setkání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.flash.success.create', 'hash' => 'd17f1ed1e331cd50e6a3e9d27e3e03ee', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Setkání bylo úspěšně založeno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.flash.success.update', 'hash' => '69397567486da10fda6782eafb3d02e0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Setkání bylo úspěšně upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.meeting.overview.action.edit', 'hash' => '5c6abe93dd537acb2b688e0c840aa08a', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.meeting.overview.action.detail', 'hash' => '37df714c1c70bdbb607573d5ca035f19', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.file.name', 'hash' => '8ecd17e2d936f17704d8dceabcbf5dd0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.file.size', 'hash' => 'fb5dce4b7f804155ddbb6d5576f4204f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Velikost', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.file.last-update-at', 'hash' => 'cfba95260d2ab3e0794b429b8bc88443', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Upraveno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.file.action.download', 'hash' => 'acd1a58207b46ceda9f5bd000fc852fd', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.file.remove.confirm', 'hash' => '89b02bd6362e9bb22264dcfed207b028', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu chcete odebrat dokument?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.meeting.edit.file.action.remove', 'hash' => 'f0c5b3a5545f59eff8f5e6dbed655eef', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.title - %s', 'hash' => 'a8eba00b89d8148d6bd03d0046a2bc79', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Detail setkání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.term', 'hash' => '23b426c45760e2e14e3d2e1b1d053ac2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.number-of-logged-users', 'hash' => '366f006e57d5a7db219b603d2becc5d1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Účastníci', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.users.log-in', 'hash' => 'bec823f3861a891b07b68d757b73eab7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přihlásit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.content.title', 'hash' => 'ecd2d94521601d27c83d5cfe73df3b25', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.files.title', 'hash' => '342a25ea372eba5a142fd9495abc4e23', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Dokumenty', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.file.name', 'hash' => '8fbdcd0b46888e6186203bcae020c10f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.file.size', 'hash' => 'afe1d37d45248cf7277ddfbf43fa6bd8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Velikost', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.file.last-update-at', 'hash' => '4a60b2fb18cc7e7c64f06f1316f4c57a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Upraveno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.file.action.download', 'hash' => '3eed764f4097db2cf821607649d8c7bf', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.flash.success.login', 'hash' => '2ec0126bd78dc264eddae7bebc9f7de9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Úspěšně jste se přihlásili.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.users.log-out.confirm', 'hash' => '457ab363c2b8b7903a2e6cbd82376460', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu se chcete odhlásit?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.users.log-out', 'hash' => 'e1df7e7237d938200069ec7514ad0d93', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odhlásit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.users.title', 'hash' => 'c5f94f244b1b4eb8faf35857f7541be4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Účastníci', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.users.log-out-user.confirm', 'hash' => 'fc7891439aba41da0e8db4f03002b557', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu chcete odhlásit účastníka?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.flash.success.logout', 'hash' => '523b473648dc82cc3fab037c091d6941', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Úspěšně jste se odhlásili.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.flash.success.logout-user', 'hash' => 'ba636d220e2d16f6e56f8f5d54b83a63', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Úspěšně jste odhlásili účastníka.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.meeting.detail.place', 'hash' => '3a06d7377da1709e25ccc68df53ce9d8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Adresa', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
